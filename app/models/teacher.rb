class Teacher < ActiveRecord::Base
  has_attached_file :avatar, styles: { normal: "440x440>" }, default_url: "/univel.jpg"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates :name, presence: true
  validates :avatar, presence: true
end
