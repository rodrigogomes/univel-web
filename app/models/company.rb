class Company < ActiveRecord::Base
  has_attached_file :logo, styles: { normal: "400x400>" }, default_url: "/univel.jpg"
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\Z/
end
