class Course < ActiveRecord::Base
  has_attached_file :banner, styles: { normal: "280x280>" }, default_url: "/univel.jpg"
  validates_attachment_content_type :banner, content_type: /\Aimage\/.*\Z/
  belongs_to :level
  has_many :registers
  validates :title, presence: true
  validates :banner, presence: true
  validates :price, presence: true
  validates :description, presence: true
  validates :duration, presence: true
  validates :level_id, presence: true
end
