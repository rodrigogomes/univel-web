class About < ActiveRecord::Base
  has_attached_file :banner, styles: { normal: "440x400>" }, default_url: "/univel.jpg"
  validates_attachment_content_type :banner, content_type: /\Aimage\/.*\Z/
  validates :title, presence: true
  validates :description, presence: true
  validates :banner, presence: true
end
