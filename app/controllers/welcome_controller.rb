class WelcomeController < ApplicationController

  def index

    @modules = UnivelModule.first
    @abouts = About.first
    @courses = Course.all
    @featureds = Course.where(featured: true)
    @stats = Stat.all
    @gallery = Galery.all
    @teachers = Teacher.all
    @company = Company.first
    @qr = RQRCode::QRCode.new("http://univel.br").to_img.resize(100,91).to_data_url
  end
end
