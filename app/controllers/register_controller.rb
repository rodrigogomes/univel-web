class RegisterController < ApplicationController
  skip_before_filter :verify_authenticity_token
  def create
    register = Register.new register_params
    if register.save
      flash[:success] = 'Cadastro realizado com sucesso!'
    else
      flash[:error] = 'Erro. Tente novamente!'
    end

    redirect_to root_path
  end

  def register_params
    params.permit(:name, :cpf, :city, :birthday, :phone, :email, :course_id, :test_date, :necessity, :school_end)
  end
end
