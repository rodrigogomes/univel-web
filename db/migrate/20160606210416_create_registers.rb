class CreateRegisters < ActiveRecord::Migration
  def change
    create_table :registers do |t|
      t.string :name
      t.string :cpf
      t.string :city
      t.string :birthday
      t.string :phone
      t.string :email
      t.integer :course_id
      t.string :test_date
      t.boolean :necessity
      t.string :school_end

      t.timestamps null: false
    end
  end
end
