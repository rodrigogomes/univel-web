class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :title
      t.attachment :banner
      t.decimal :price
      t.text :description
      t.integer :duration
      t.integer :level_id

      t.timestamps null: false
    end
  end
end
