class CreateUnivelModules < ActiveRecord::Migration
  def change
    create_table :univel_modules do |t|
      t.boolean :featureds, default: true
      t.boolean :abouts, default: true
      t.boolean :courses, default: true
      t.boolean :stats, default: true
      t.boolean :gallery, default: true
      t.boolean :teachers, default: true
      t.boolean :promos, default: true
      t.boolean :partners, default: true
      t.boolean :blog, default: true
      t.boolean :register, default: true
      t.boolean :maps, default: true
      t.boolean :footer, default: true
      t.boolean :header, default: true

      t.timestamps null: false
    end
  end
end
