class CreateAbouts < ActiveRecord::Migration
  def change
    create_table :abouts do |t|
      t.string :title
      t.text :description
      t.string :video
      t.attachment :banner

      t.timestamps null: false
    end
  end
end
