class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :company_name
      t.string :company_address
      t.string :company_phone
      t.string :facebook
      t.string :twitter

      t.timestamps null: false
    end
  end
end
