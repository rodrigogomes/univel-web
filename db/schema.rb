# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160607011524) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "abouts", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "video"
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string   "company_name"
    t.string   "company_address"
    t.string   "company_phone"
    t.string   "facebook"
    t.string   "twitter"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "company_email"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  create_table "courses", force: :cascade do |t|
    t.string   "title"
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
    t.decimal  "price"
    t.text     "description"
    t.integer  "duration"
    t.integer  "level_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.boolean  "featured"
  end

  create_table "galeries", force: :cascade do |t|
    t.string   "title"
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "levels", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "registers", force: :cascade do |t|
    t.string   "name"
    t.string   "cpf"
    t.string   "city"
    t.string   "birthday"
    t.string   "phone"
    t.string   "email"
    t.integer  "course_id"
    t.string   "test_date"
    t.boolean  "necessity"
    t.string   "school_end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stats", force: :cascade do |t|
    t.string   "title"
    t.integer  "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "teachers", force: :cascade do |t|
    t.string   "name"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "univel_modules", force: :cascade do |t|
    t.boolean  "featureds",  default: true
    t.boolean  "abouts",     default: true
    t.boolean  "courses",    default: true
    t.boolean  "stats",      default: true
    t.boolean  "gallery",    default: true
    t.boolean  "teachers",   default: true
    t.boolean  "promos",     default: true
    t.boolean  "partners",   default: true
    t.boolean  "blog",       default: true
    t.boolean  "register",   default: true
    t.boolean  "maps",       default: true
    t.boolean  "footer",     default: true
    t.boolean  "header",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

end
